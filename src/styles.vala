namespace Spacelaunch{

    public Gtk.CssProvider[] chargementCss(){

        Gtk.CssProvider[] listeCss;
        listeCss = {};

        var css0 = new Gtk.CssProvider();
        uint8[] fondVert = "* { background: #00AB63; border-radius: 10px; color: white;}".data;
        css0.load_from_data(fondVert);
        listeCss += css0;

        var css1 = new Gtk.CssProvider();
        uint8[] fondGris = "* { background: #786FA2; border-radius: 10px; color: white;}".data;
        css1.load_from_data(fondGris);
        listeCss += css1;

        var css2 = new Gtk.CssProvider();
        uint8[] fondOrange = "* { background: #518EDC; border-radius: 10px; color: white;}".data;
        css2.load_from_data(fondOrange);
        listeCss += css2;

        var css3 = new Gtk.CssProvider();
        uint8[] fondRouge = "* { background: #E12E2E; border-radius: 10px; color: white;}".data;
        css3.load_from_data(fondRouge);
        listeCss += css3;

        var css4 = new Gtk.CssProvider();
        uint8[] labelCAR = "* { color: #0b9800; }".data;
        css4.load_from_data(labelCAR);
        listeCss += css4;

        var css5 = new Gtk.CssProvider();
        uint8[] fondTransparent = "* { background: #00000000; } *:hover { background: #00000000; } *:selected { background: #00000000; }".data;
        css5.load_from_data(fondTransparent);
        listeCss += css5;

        var css6 = new Gtk.CssProvider();
        uint8[] coinsArrondisG = "* { border-top-left-radius: 10px; } * { border-bottom-left-radius: 10px; }".data;
        css6.load_from_data(coinsArrondisG);
        listeCss += css6;

        var css7 = new Gtk.CssProvider();
        uint8[] coinsArrondisD = "* { border-top-right-radius: 10px; } * { border-bottom-right-radius: 10px; }".data;
        css7.load_from_data(coinsArrondisD);
        listeCss += css7;

        var css8 = new Gtk.CssProvider();
        uint8[] fondOrbite = "* { background: #518edc; border-radius: 6px; color: white;}".data;
        css8.load_from_data(fondOrbite);
        listeCss += css8;

        /*var css9 = new Gtk.CssProvider();
        //uint8[] statusGo = "* { background: #00AB63; color: white; } * { border-top-left-radius: 10px; } * { border-top-right-radius: 10px; } * { transform: rotate(270deg); }".data;
        uint8[] statusGo = "* { transform: rotate(270deg); }".data;
        css9.load_from_data(statusGo);
        listeCss += css9;*/

        return listeCss;
    }
}
