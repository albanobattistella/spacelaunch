namespace Spacelaunch{

    public class Requete : Object {

        public Window win {get;set;}

        public Requete (Window win) {
            Object (
                win: win
            );
        }

        public string[] requestUpcomingLaunchNext(string[] listeExistante, bool[] listeFiltres) {

            string[] list = listeExistante;
            while(list.length < win.nbLancement && win.offset < 100){
                string url = "";
                    if(!win.dev) url = "https://spacelaunchnow.me/api/ll/2.2.0/launch/upcoming/?offset="+win.offset.to_string();
                if(win.dev) url = Environment.get_user_data_dir()+"/message_"+win.offset.to_string();
                string[] listeTemp = analyseListe(url);
                string erreurServeur = lectureCle("detail",listeTemp[1]);
                bool erreur = false;
                if(erreurServeur.length > 1){
                      if(lectureCle("detail",listeTemp[1]).substring(0,21)=="Request was throttled") erreur = true;
                }
                if(!erreur){
                    string[] listeTemp0 = filtreListe(listeTemp, listeFiltres);
                    if(listeTemp[0]!="null") for(int i=1;i<listeTemp0.length;i++) list+=listeTemp0[i];
                    win.offset+=10;
                }
                else{
                    //stderr.printf("Erreur serveur\n");
                }
            }
            return list;
        }

        private string[] analyseListe(string url) {

            string[] list = {};
            string chaine = "";
            int longueurMessage = 0;

            if(!win.dev){
                Soup.Session session = new Soup.Session.with_options ("timeout", 5);
                Soup.Message messageSoup = new Soup.Message ("GET", url);
                session.send_message (messageSoup);
                chaine = (string) messageSoup.response_body.data;
                longueurMessage = (int) messageSoup.response_body.length;
            }
            else{
                var dis = FileStream.open(url, "r");
                chaine = dis.read_line();
                longueurMessage = chaine.length;
            }

            if(longueurMessage!=0){
                //SÉPARATION BLOCS
                bool stop = false;
                int debut = 0;
                //RECHERCHE DÉBUT BLOCS
                for(int i = 12;i < chaine.length ; i++){
                    if(chaine.substring(i-11,11) == "\"results\":[" && !stop){
                        debut = i;
                        stop = true;
                    }
                }
                //DÉCOUPAGE DES BLOCS

                int acc = 0;
                int debutBloc = 0;
                int finBloc = 0;
                int nbBloc = 0;
                for(int i = debut; i < chaine.length ; i++){
                    if(chaine[i]=='{'){
                        if(acc==0) debutBloc = i;
                        acc++;
                    }
                    if(chaine[i]=='}'){
                        acc--;
                        if(acc==0){
                            finBloc = i;
                            list += chaine.substring(debutBloc,finBloc-debutBloc);
                            nbBloc++;
                        }
                    }
                }
            }
            else{
                list[0]="null";
            }

            return list;
        }

        private string[] filtreListe(string[] listeEntree, bool[] listeFiltres) {

            //VÉRIFICATION FILTRES
            string[] listeSortie = {""};

            for(int i = 0; i < listeEntree.length-1; i++){
                bool filtreOK = false;
                string codePays = lectureCle("country_code", listeEntree[i]);
                if(codePays=="CHN"){if(listeFiltres[0]) filtreOK = true;}
                else if(codePays=="GUF"){if(listeFiltres[1]) filtreOK = true;}
                else if(codePays=="IND"){if(listeFiltres[2]) filtreOK = true;}
                else if(codePays=="JPN"){if(listeFiltres[3]) filtreOK = true;}
                else if(codePays=="KAZ"){if(listeFiltres[4]) filtreOK = true;}
                else if(codePays=="NZL"){if(listeFiltres[5]) filtreOK = true;}
                else if(codePays=="RUS"){if(listeFiltres[6]) filtreOK = true;}
                else if(codePays=="USA"){if(listeFiltres[7]) filtreOK = true;}
                else if(listeFiltres[8]) filtreOK = true;
                if(filtreOK) listeSortie += listeEntree[i];
            }

            return listeSortie;
        }

        public int analyseId(string id, string[] list) {

            int num = 0;

            for(int i=0;i<list.length-1;i++){
                if(lectureCle("id",list[i]) == id) num = i;
            }

            return num;
        }

        public string requestData(string url) {

            string chaine = "";
            bool boucle = true;
            while(boucle){
                if(win.dev){
                    message(url);
                    string[] fichier = url.split("/");
                    string fichierDev = Environment.get_user_data_dir()+"/"+fichier[fichier.length-2];
                    message(fichierDev);
                    var dis = FileStream.open(fichierDev, "r");
                    chaine = dis.read_line();
                }
                else {
                    var session = new Soup.Session.with_options ("timeout", 2);
                    var message = new Soup.Message ("GET", url);
                    session.send_message (message);
                    chaine = (string) message.response_body.data;
                }
                string erreurServeur = lectureCle("detail",chaine);
                if(erreurServeur.length > 1){
                    if(lectureCle("detail",chaine).substring(0,21)=="Request was throttled"){
                        boucle = true;
                    }
                    else{
                        boucle = false;
                    }
                }
                else{
                    boucle = false;
                }
            }
            return chaine;
        }
    }
}
